#!/usr/bin/env python3

from pymysql import connect, err, sys, cursors

__doc__ = """
Utility function to create language model base from ONet DB.

Using O*Net database we will extract some of the information to create
a basic description per each major. This will include tasks, dwas,
job titles, job titles reported, occupations.

We will get file per category stored in the data directory.

Requirements
============
 - Download O*Net database from O*Net resources site.
 - Install MySQL and create database.
 - Create user and modify connection properties if required.
"""
majors = {}
def check_major(major):
    """ Check if major already exist in dictionary otherwise add it. """

    if not major in majors:
        majors[major] = []

def occupations(cursor):
    """ Fetch all occupations grouped by major """

    occupations = """SELECT
     occupation_data.title, occupation_data.description,
      SUBSTRING(occupation_data.onetsoc_code, 1, 2) as major
      FROM occupation_data
      """
    cursor.execute( occupations )

    data = cursor.fetchall()
    for row in data:
        major = row['major']
        check_major(major)

        majors[major].append( row['title'] )
        majors[major].append( row['description'] )


def reported_titles(cursor):
    """ Fetch reported titles grouped by major """

    query = """SELECT reported_job_title as title, SUBSTRING(onetsoc_code, 1, 2) as major FROM sample_of_reported_titles """
    cursor.execute( query )
    data = cursor.fetchall()
    for row in data:
        major = row['major']
        check_major(major)

        majors[major].append( row['title'] )

def alternate_titles(cursor):
    """ Fetch alternated titles, grouped by major """

    query = """SELECT alternate_title as title, SUBSTRING(onetsoc_code, 1, 2) as major FROM alternate_titles"""
    cursor.execute( query )
    data = cursor.fetchall()
    for row in data:
        major = row['major']
        check_major(major)

        majors[major].append( row['title'] )

def skills(cursor):
    """ Fetch skills grouped by major """

    query = """
    SELECT s.element_id, SUBSTRING(s.onetsoc_code, 1, 2) as major, `c`.`description`
    FROM skills s INNER JOIN content_model_reference c on s.element_id = c.element_id
    GROUP BY major, s.element_id;
    """

    cursor.execute(query)
    data = cursor.fetchall()
    for row in data:
        major = row['major']
        check_major( major )
        majors[major].append( row['description'] )

def append_data(data, fields):
    """ Append data to majors dictionary.

    Each row in data contain multiples fields attached to a major.
    """
    for row in data:
        major = row['major']
        check_major( major )
        for field in fields:
            if not row[field] in majors[major]:
                majors[major].append( row[field] )


def abilities(cursor):
    """ Fetch required abilities per major """

    query = """
    SELECT s.element_id, SUBSTRING(s.onetsoc_code, 1, 2) as major, `c`.`description`
    FROM abilities s INNER JOIN content_model_reference c on s.element_id = c.element_id
    GROUP BY major, s.element_id;
    """
    cursor.execute(query)
    data = cursor.fetchall()
    append_data(data, ['description'])

#def educations(cursor):
#    query = """
#    SELECT s.element_id, SUBSTRING(s.onetsoc_code, 1, 2) as major, `c`.`description`
#    FROM education_training_experience s INNER JOIN content_model_reference c on s.element_id = c.element_id
#    GROUP BY major, s.element_id;
#    """
#    cursor.execute(query)
#    data = cursor.fetchall()
#    append_data(data, ['description'])

def tasks(cursor):
    """ Task to do in each job category """

    query = """
    SELECT SUBSTRING(s.onetsoc_code, 1, 2) as major, task
    FROM emerging_tasks s;
    """
    cursor.execute(query)
    data = cursor.fetchall()
    append_data(data, ['task'])

    query = """
    SELECT SUBSTRING(s.onetsoc_code, 1, 2) as major, dwa_title as title, description
    FROM tasks_to_dwas s LEFT JOIN dwa_reference d ON d.dwa_id = s.dwa_id LEFT JOIN content_model_reference c ON c.element_id =
    d.element_id;
    """
    cursor.execute(query)
    data = cursor.fetchall()
    append_data(data, ['title', 'description'])

def knowledges(cursor):
    """ Type of knowledge required to perform in each kind of job """

    query = """
    SELECT s.element_id, SUBSTRING(s.onetsoc_code, 1, 2) as major, `c`.`description`
    FROM knowledge s INNER JOIN content_model_reference c on s.element_id = c.element_id
    GROUP BY major, s.element_id;
    """
    cursor.execute(query)
    data = cursor.fetchall()
    append_data(data, ['description'])

def tools(cursor):
    """ Tools used in each job category """

    query = """
    SELECT SUBSTRING(s.onetsoc_code, 1, 2) as major, s.t2_example as title
    FROM tools_and_technology s;
    """
    cursor.execute(query)
    data = cursor.fetchall()
    append_data(data, ['title'])

def work_activities(cursor):
    """ Other kind of work activities to do in each job category """

    query = """
    SELECT s.element_id, SUBSTRING(s.onetsoc_code, 1, 2) as major, `c`.`description`
    FROM work_activities s INNER JOIN content_model_reference c on s.element_id = c.element_id
    GROUP BY major, s.element_id;
    """
    cursor.execute(query)
    data = cursor.fetchall()
    append_data(data, ['description'])

def work_styles(cursor):
    """ Style of work for each category """

    query = """
    SELECT s.element_id, SUBSTRING(s.onetsoc_code, 1, 2) as major, `c`.`description`
    FROM work_styles s INNER JOIN content_model_reference c on s.element_id = c.element_id
    GROUP BY major, s.element_id;
    """
    cursor.execute(query)
    data = cursor.fetchall()
    append_data(data, ['description'])

def work_values(cursor):
    """ Work values in each category """

    query = """
    SELECT s.element_id, SUBSTRING(s.onetsoc_code, 1, 2) as major, `c`.`description`
    FROM work_values s INNER JOIN content_model_reference c on s.element_id = c.element_id
    GROUP BY major, s.element_id;
    """
    cursor.execute(query)
    data = cursor.fetchall()
    append_data(data, ['description'])


def main():
    """
    Connect to MySQL localhost and fetch each type of data, after that write each
    major description to its own file.
    """
    conn = connect( host='localhost', user='onet', passwd='onet_123', db="onet_22_1")
    try:
        with conn.cursor( cursors.DictCursor ) as cursor:
            occupations(cursor)
            reported_titles(cursor)
            alternate_titles(cursor)
            skills(cursor)
            abilities(cursor)
            tasks(cursor)
            tools(cursor)
            knowledges(cursor)
            work_activities(cursor)
            work_styles(cursor)
            work_values(cursor)

    finally:
        conn.close()

    for major, content in majors.items():
        with open("data/descriptions/major_" + major + ".txt" , "w") as f:
            try:
                f.write( " ".join(content) )
            except:
                print("Error: ", sys.exc_info()[0])

if __name__ == "__main__" : main()
