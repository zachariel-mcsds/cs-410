import logging

from os import path
from django.shortcuts import render
from django.http import HttpResponse

from django.contrib.auth.models import User, Group

from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from classification.serializers import JobSerializer
from classification.models import Job

from classifier import onet_model as classifier

logger = logging.getLogger(__name__)

# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the classification index.")

class JobViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Job.objects.all().order_by('-created_at')
    serializer_class = JobSerializer

    def create(self, request):
        base_dir = path.dirname(__file__)
        file_path = path.abspath(path.join(base_dir, "..", "..", "classifier", "models", "current.model"))
        logger.info(str(file_path))
        return super().create(request)

    @list_route(methods=['post', 'get'])
    def classify(self, request, pk=None):
        """
        API method to classify a document
        """
        reference_number = request.POST['reference_number']
        content          = request.POST['content']
        job = Job(reference_number=reference_number, content=content)
        base_dir = path.dirname(__file__)
        file_name = 'current.model'
        file_path = path.abspath(path.join(base_dir, "..", "classifier", "models", file_name))
        clf = classifier.OnetModel().load(file_path)
        job.category = clf.predict([content])[0]
        job.save()
        #return Response(str(file_path))
        return Response(str(job.category))


