Hi this is Zachariel Norzagaray, this presentation is related
with Text Information Systems course in the MCS-Data Science track
from University Of Illinois at Urbana-Champaign.

Development of this project is to apply the knowledge gain in this course,
so it's structure is to include modules related to web crawling, document
clustering and document classification.

The problem: create an API able to classify a job description into one of
several categories from the Standard Occupational Classification (SOC) system.

As we can see in this image, this classification include different layer, they
go from a major group to a detailed occupation.

This project is focused in the major group, I will like to do a deeper classification
that go over the full tree structure until detailed occupation, I hope this can be done
in the future.

You can get more information about SOC system in SOC page and ONET site.

--------------------

Before start working we will need to clone the projects repository hosted in bitbucket,
to do it we will open the console and type the following command:

$ git clonet git@bitbucket.org:zachariel-mcsds/cs-410.git

next get into project directory
$ cd cs-410

---------------------
Let's start with Scraper module

Scraper use careersonestop.org as the hub page, this page link to several
job boards across in the united states.

To run this module we need to follow this instructions:

$ python3 scrape/scraper.py

This will start scraping and save documents into data/scraper directory, as we can see.

This process will take hours and hours to finish, so let's cancel it and see some of the downloaded
documents.

For this project I already scrape and create a dataset from downloaded documents, let's extract this
files:

$ cd data
$ tar xvfz onet.tgz

Let's explore our dataset structure:
$ tree onet -d

----------------------

Next we will check our clustering module

$ cd ..

to run this module we just need to run:

$ python3 clustering/kmeans.py

Once the script finished we will get some stats about clusters, and what are the relevants keywords
for each cluster, in this case Kmeans use the TFIDF weighting to determine similarity between documents.

This is an unsupervised clustering so, at this point we cannot rely that each cluster represent one
of the majors groups, but we can get an idea of the words distribution.

------------------------

The classifier module:

to run the model training we need to run:

$ python3 classifier/onet_model.py

This module is the core of our classification system, it use LinearSVC algorithm, Support Vector Classifier
with Linear Kernel, and for text tranformation we do stemming, ngram tokenization (1 and 2 words), and TF-IDF
weighting.

Before select this model, I did some run with different configuration and different algorithms, as we can see
in this image.

The selected configuration was the best, so far, getting a 0.864 accuracy score as we can see in the image.

Once the script finished it will store the model to classifiers/models directory.

And we're ready to start classifying our documents, to do that we will start our API server:

$ python3 manage.py runserver

To make this faster I have an script that will post documents to the API and print the category.

$ python3 post_jobs_test.py

Last we can open the API dashboard to review our results.

---------------------------





