#!/usr/bin/env python3

import pickle
import os

from sklearn.datasets import load_files
from nltk.stem.snowball import SnowballStemmer

from time import time
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.model_selection import GridSearchCV

from sklearn.preprocessing import LabelEncoder
from sklearn import metrics

from sklearn.datasets import load_files

if __name__ == '__main__':
    from tokenizer import Tokenizer
    from tokenizer import identity
else:
    from .tokenizer import Tokenizer
    from .tokenizer import identity

class OnetModel():
    """Wrapper class over our trained model.

    Class to represent our language model to classify job documents
    into one of the O*Net major categories.

    Once our model is trained we save it to disk and load it when needed
    using pickle module.

    This class is used in our API.

    Paramters:

    model: optional
        In case of provided, it is the serialized model with our best parameters.
    """
    def __init__(self, model=None):
        self.tokenizer  = Tokenizer()
        self.data_train = None
        self.data_test  = None
        self.classifier = None
        self.outpath    = None
        self.model      = model
        self.score      = 0.0

    @property
    def target_names(self):
        """O*Net categories in our training data."""
        if not self.data_train: return []
        return self.data_train.target_names

    def run(self, data_train, data_test):
        """Run our trainig stage.

        Train and cross-validate our model."""
        self.data_train = data_train
        self.data_test  = data_test

        self.train()
        self.test()
        return self

    def train(self):
        """Training stage"""
        print('=' * 80)
        print("Training: ")
        print('_' * 80)
        #vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5,
        #                             stop_words='english')
        #X_vectorized = vectorizer.fit_transform(X)

        print(self.model)
        t0 = time()
        self.model.fit(self.data_train.data, self.data_train.target)
        self.model.labels_ = self.data_train.target_names
        train_time = time() - t0
        print("train time: %0.3fs" % train_time)

        return self

    def test(self):
        """Testing stage, cross-validation.

        Print accuracy score and metrics report: Precision, Recall, F1-Score."""
        t0 = time()
        y_pred = self.model.predict(self.data_test.data)
        test_time = time() - t0
        print("test time:  %0.3fs" % test_time)

        self.score = metrics.accuracy_score(self.data_test.target, y_pred)
        print("accuracy:   %0.3f" % self.score)

        if hasattr(self.model, 'coef_'):
            print("dimensionality: %d" % self.model.coef_.shape[1])
            print("density: %f" % density(self.model.coef_))

            print("top 10 keywords per class:")
            for i, label in enumerate(self.data_train.target_names):
                top10 = np.argsort(self.model.coef_[i])[-10:]
                print(trim("%s: %s" % (label, " ".join(feature_names[top10]))))

        print("classification report:")
        print(metrics.classification_report(self.data_test.target, y_pred,
                                            target_names=self.data_train.target_names))

        return self

    def persist(self, path):
        """Serialize and persist model to disk.

        Parameters:

        path: string
            Path to store model
        """
        with open(path, 'wb') as f:
            pickle.dump(self.model, f)
            print("Model written out to {}".format(path))

    def restore(self, path):
        """Read and deserialize model.

        Parameters:

        path: string
            Path where model was saved.
        """
        with open(path, 'rb') as f:
            self.model = pickle.load(f)
        return self

    def predict(self, documents):
        """Predict O*Net category for documents.

        Parameters:

        documents: string/array
            Single document or list of documents to predict.

        Returns:

        categories: array
            Category associated with each document or None if missing model.
        """
        if not self.model: return None
        if not isinstance(documents, list): documents = [documents]

        results = []
        predictions = self.model.predict(documents)
        for prediction in predictions:
            results.append(self.model.labels_[prediction])

        return results

    @classmethod
    def load(cls, path):
        """Static method to load model from disk

        Parameters:
        path: string
        Path to model.

        Returns:
        object: OnetModel
        OnetModel instance with loaded model.
        """
        classifier = cls().restore(path)
        return classifier



##https://bbengfort.github.io/tutorials/2016/05/19/text-classification-nltk-sckit-learn.html
##https://gist.github.com/bbengfort/044682e76def583a12e6c09209c664a1

def load_data(train_path='data/onet/train',
              test_path='data/onet/test',
              random=0):
    """Load training and test datasets"""
    data_train = load_files(train_path, encoding="utf8", random_state=random)
    data_test  = load_files(test_path,  encoding="utf8", random_state=random)

    return (data_train, data_test)

def main():
    """Script to train our model"""
    base_dir = os.path.dirname(__file__) + "/models/"
    data_train, data_test = load_data()

    snowball = SnowballStemmer("english")

    #classifier = LinearSVC(penalty='l1', dual=False, tol=1e-3)
    classifier = LinearSVC(penalty='l2', loss="squared_hinge")

    model = Pipeline([
                ('vectorize', Tokenizer(stemmer=snowball)),
                ('tfidf', TfidfVectorizer(tokenizer=identity, preprocessor=None, lowercase=False, ngram_range=(1,2))),
                ('clf', classifier)
    ])

    o_model = OnetModel(model)
    o_model.run(data_train, data_test)
    file_name = 'current.model'
    file_name = "%s_%s.model" % (o_model.score, id(o_model))
    o_model.persist( base_dir + file_name)

if __name__ == '__main__' : main()
