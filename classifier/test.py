# Author: Peter Prettenhofer <peter.prettenhofer@gmail.com>
#         Olivier Grisel <olivier.grisel@ensta.org>
#         Mathieu Blondel <mathieu@mblondel.org>
#         Lars Buitinck
# License: BSD 3 clause

from __future__ import print_function

import logging
import numpy as np
from optparse import OptionParser
import sys
from time import time
import matplotlib.pyplot as plt

if __name__ == '__main__':
    from tokenizer import identity
    from tokenizer import Tokenizer
else:
    from .tokenizer import identity
    from .tokenizer import Tokenizer
from nltk.stem.snowball import SnowballStemmer

from sklearn.datasets import fetch_20newsgroups
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.linear_model import RidgeClassifier
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import Perceptron
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestCentroid
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.extmath import density
from sklearn import metrics


def main():
# Display progress logs on stdout
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)s %(message)s')


# parse commandline arguments
    op = OptionParser()
    op.add_option("--report",
                  action="store_true", dest="print_report",
                  help="Print a detailed classification report.")
    op.add_option("--top10",
                  action="store_true", dest="print_top10",
                  help="Print ten most discriminative terms per class"
                       " for every classifier.")


    def is_interactive():
        return not hasattr(sys.modules['__main__'], '__file__')

# work-around for Jupyter notebook and IPython console
    argv = [] if is_interactive() else sys.argv[1:]
    (opts, args) = op.parse_args(argv)
    if len(args) > 0:
        op.error("this script takes no arguments.")
        sys.exit(1)

    #print(__doc__)
    #op.print_help()
    print()


# #############################################################################
# Load some categories from the training set
    categories = None

    print("Loading O*Net major categories.")

    data_train = load_files('/Users/zachariel/pdn_data/onet_dataset/train', encoding="utf8")
    data_test  = load_files('/Users/zachariel/pdn_data/onet_dataset/test', encoding="utf8")
    print('data loaded')

    # order of labels in `target_names` can be different from `categories`
    target_names = data_train.target_names
    categories = data_train.target_names


    def size_mb(docs):
        return sum(len(s.encode('utf-8')) for s in docs) / 1e6

    data_train_size_mb = size_mb(data_train.data)
    data_test_size_mb = size_mb(data_test.data)

    print("%d documents - %0.3fMB (training set)" % (
        len(data_train.data), data_train_size_mb))
    print("%d documents - %0.3fMB (test set)" % (
        len(data_test.data), data_test_size_mb))
    print("%d categories" % len(categories))
    print()

# split a training set and a test set
    y_train, y_test = data_train.target, data_test.target

    print("Extracting features from the training data using a sparse vectorizer")
    t0 = time()

    #vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5,
    #                             stop_words='english')

    snowball = SnowballStemmer("english")
    tokenizer = Tokenizer(stemmer=snowball)
    #tokenizer = Tokenizer()
    vectorizer = CountVectorizer(tokenizer=tokenizer) #, ngram_range=(1,2))
    X = vectorizer.fit_transform(data_train.data)
    transformer = TfidfTransformer(smooth_idf=False)
    X_train = transformer.fit_transform(X)

    #snowball = SnowballStemmer("english")
    #tokenizer = Tokenizer(stemmer=snowball)
    #vectorizer = TfidfVectorizer(tokenizer=tokenizer.tokenize, preprocessor=None, lowercase=False, ngram_range=(1,2))

    #X_train = vectorizer.fit_transform(data_train.data)

    duration = time() - t0
    print("done in %fs at %0.3fMB/s" % (duration, data_train_size_mb / duration))
    print("n_samples: %d, n_features: %d" % X_train.shape)
    print()

    print("Extracting features from the test data using the same vectorizer")
    t0 = time()
    #X_test = vectorizer.transform(data_test.data)
    X_test = vectorizer.transform(data_test.data)
    X_test = transformer.transform(X_test)
    duration = time() - t0
    print("done in %fs at %0.3fMB/s" % (duration, data_test_size_mb / duration))
    print("n_samples: %d, n_features: %d" % X_test.shape)
    print()

# mapping from integer feature name to original token string
    feature_names = vectorizer.get_feature_names()

    if feature_names:
        feature_names = np.asarray(feature_names)


    def trim(s):
        """Trim string to fit on terminal (assuming 80-column display)"""
        return s if len(s) <= 80 else s[:77] + "..."


# #############################################################################
# Benchmark classifiers
    def benchmark(clf, raw_training=False):
        print('_' * 80)
        print("Training: ")
        print(clf)
        t0 = time()
        if raw_training:
            clf.fit(data_train.data, data_train.target)
        else:
            clf.fit(X_train, y_train)
        train_time = time() - t0
        print("train time: %0.3fs" % train_time)

        t0 = time()
        if raw_training:
            pred = clf.predict(data_test.data)
        else:
            pred = clf.predict(X_test)

        test_time = time() - t0
        print("test time:  %0.3fs" % test_time)

        score = metrics.accuracy_score(y_test, pred)

        print("accuracy:   %0.3f" % score)

        if hasattr(clf, 'coef_'):
            print("dimensionality: %d" % clf.coef_.shape[1])
            print("density: %f" % density(clf.coef_))

            if opts.print_top10 and feature_names is not None:
                print("top 10 keywords per class:")
                for i, label in enumerate(target_names):
                    top10 = np.argsort(clf.coef_[i])[-10:]
                    print(trim("%s: %s" % (label, " ".join(feature_names[top10]))))
            print()

        if opts.print_report:
            print("classification report:")
            print(metrics.classification_report(y_test, pred,
                                                target_names=target_names))

        print()
        clf_descr = str(clf).split('(')[0]
        return clf_descr, score, train_time, test_time


    results = []
#for clf, name in (
#        (RidgeClassifier(tol=1e-2, solver="lsqr"), "Ridge Classifier"),
#        (Perceptron(n_iter=50), "Perceptron"),
#        (PassiveAggressiveClassifier(n_iter=50), "Passive-Aggressive"),
#        (KNeighborsClassifier(n_neighbors=10), "kNN"),
#        (RandomForestClassifier(n_estimators=100), "Random forest")):
#    print('=' * 80)
#    print(name)
#    results.append(benchmark(clf))


    #clf_sets = [(LinearSVC(penalty='l1', loss='squared_hinge', dual=False,
    #                   tol=1e-3),
    #         np.logspace(-2.3, -1.3, 10), X_1, y_1),
    #        (LinearSVC(penalty='l2', loss='squared_hinge', dual=True,
    #                   tol=1e-4),
    #         np.logspace(-4.5, -2, 10), X_2, y_2)]

    cs = np.logspace(-4.5, -2, 10)
    cs = np.logspace(-0.4, 0.1, 10)
    clf_sets = []
    cs = [1]
    for c in cs:
      clf_sets.append( LinearSVC(C=c) )
      clf_sets.append( LinearSVC(C=c, penalty='l1', loss='squared_hinge', dual=False, tol=1e-3) )
      clf_sets.append( LinearSVC(C=c, penalty='l2', loss='squared_hinge', dual=True, tol=1e-4) )
    #clf_sets = {'one': LinearSVC(C=c),
    #  'two':  LinearSVC(C=c, penalty='l1', loss='squared_hinge', dual=False, tol=1e-3),
    #  'three': LinearSVC(C=c, penalty='l2', loss='squared_hinge', dual=True, tol=1e-4),
    #}

    #for classifier in clf_sets:
      #print('=' * 80)
      #model = Pipeline([
      #            ('clf', classifier)
      #])
      #results.append(benchmark(classifier))

    #vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5,
    #                             stop_words='english')

    for penalty in ["l2", "l1"]:
        #if penalty == "l2" : continue
        print('=' * 80)
        print("%s penalty" % penalty.upper())
        # Train Liblinear model
        results.append(benchmark(LinearSVC(penalty=penalty, dual=False,
                                           tol=1e-3)))

        # Train SGD model
        results.append(benchmark(SGDClassifier(alpha=.0001, n_iter=50,
                                               penalty=penalty)))

    # Train SGD with Elastic Net penalty
    print('=' * 80)
    print("Elastic-Net penalty")
    results.append(benchmark(SGDClassifier(alpha=.0001, n_iter=50,
                                           penalty="elasticnet")))

    # Train NearestCentroid without threshold
    print('=' * 80)
    print("NearestCentroid (aka Rocchio classifier)")
    results.append(benchmark(NearestCentroid()))

    # Train sparse Naive Bayes classifiers
    print('=' * 80)
    print("Naive Bayes")
    results.append(benchmark(MultinomialNB(alpha=.01)))
    results.append(benchmark(BernoulliNB(alpha=.01)))


    print("LinearSVC with L1-based feature selection")
    # The smaller C, the stronger the regularization.
    # The more regularization, the more sparsity.
    results.append(benchmark(Pipeline([
      ('feature_selection', SelectFromModel(LinearSVC(penalty="l1", dual=False,
                                                      tol=1e-3))),
      ('classification', LinearSVC(penalty="l2"))])))

    # make some plots

    indices = np.arange(len(results))

    results = [[x[i] for x in results] for i in range(4)]

    clf_names, score, training_time, test_time = results
    training_time = np.array(training_time) / np.max(training_time)
    test_time = np.array(test_time) / np.max(test_time)

    plt.figure(figsize=(12, 8))
    plt.title("Score")
    plt.barh(indices, score, .2, label="score", color='navy')
    plt.barh(indices + .3, training_time, .2, label="training time",
             color='c')
    plt.barh(indices + .6, test_time, .2, label="test time", color='darkorange')
    plt.yticks(())
    plt.legend(loc='best')
    plt.subplots_adjust(left=.25)
    plt.subplots_adjust(top=.95)
    plt.subplots_adjust(bottom=.05)

    for i, c in zip(indices, clf_names):
        plt.text(-.3, i, c)

    plt.show()


if __name__ == '__main__' : main()
