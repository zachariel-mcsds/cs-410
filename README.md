# MCS-DS / CS410 - Text Information Systems
## [Code Documentation](https://zachariel-mcsds.bitbucket.io/cs410/index.html)

### Fall 2017 Course project: SOC classification

This project has as goal apply the knowledge gain during **Text Information Systems** course, to do that we will
go over what could be a real world process: crawling the web, groups documents, and classify them. At the end,
provide an API that is able to receive a text document and return the category it belongs to.

Job description or job occupations are classified according to **Standard Occupational Classification (SOC)**,
ONET-SOC taxonomy structure categories into different levels, tree based structure, according requirements,
skills, work activities, work description and others. As show in image below, tree is divided in **major groups**,
**minor groupss**, **broad occupations** and **detailed SOC occupation**.

![ONET-SOC Taxonomy](images/onet_taxonomy.png)

One of the tasks that require more time and human involvement is the creation of **gold standard** dataset that helped
train our model. This was a really tedious task because bigger the corpora, bigger the amount of time that a person
or group of persons need to spend reading and verifing that document belongs to the right category, being said that,
probably the gold dataset used to train this model is not 100% perfect, some categories could get missclassified
documents causing accuracy scores not being the best wanted.

Ideally will should predict the entire path until get **specific occupation**, but for education purposes and
to learn how different classification algorithms behave, this project is focused only in predict the first layer
of the structure: **major groups**.

![SOC majors](images/soc_majors.png)

Job document were fetched from several sites, using [**_www.careeronestop.org_**](https://www.careeronestop.org/)
as hub page, it link to different job boards, the amount of data retrieved from these sites was tremendous,
but was reduced trying to set the same amount of documents per category to get an unbiased dataset as much as possible.

This project has been developed using **Python3** and great educational projects like:

- [**Scikit-Learn**](http://scikit-learn.org/stable/index.html): best known as **sklearn**, is a toolkit that
offers great documentation and educational examples related with machine learning. Used to develop the NLP side
of this project.
- [**Natural Language Toolkit - NLTK**](http://www.nltk.org/): This is a very known toolkit for NLP, it was used
to preprocess documents, primary to enable stemming and lemmatization because sklearn doesn't not provide that.

This project is divided in four modules:

- [Scraper](#markdown-header-scraper)
- [Clustering](#markdown-header-clustering)
- [Classifier](#markdown-header-classifier)
- [Classification API](#markdown-header-classification-api)

## Getting Started

### Requirements

1. Python3
2. Git client

### Installation

1. Clone this repository: ```git clone git@bitbucket.org:zachariel-mcsds/cs-410.git```
2. Into project directory: ```pip install -r requirements.txt```

## Scraper

This is a simple crawler to fetch job documents, to approach this we will make use of BeautifulSoup and
requests packages, we set our root page to www.careeronestop.org search page, we will need a combination
of SOC Code and job title to perform searches. The basic procedure is as follow:

1. For each SOC category Perform a job search.
2. Parse results page and fetch JDP urls.
3. For each link in results page, create a new JDP request.
4. Read JDP, parse and save content to disk.
5. Move to next page of results.

Scraper will store each file under the major directory, because search is performed using SOC occupation code
as keyword, but at this point we aren't sure if results we're getting were correctly classified.

### How to use

As simple as type in console:
```
python3 scrape/scraper.py
```

It will start the process and output each file under a major group directory
in ```data/scraper/``` directory, it is limited to process only the first page
of results which will contain at must 500 documents.

## Clustering

To apply clustering methods learned at class, we cluster our fetched documents to see
the structure and commons words for each category. We can apply clustering per majors,
minor or broad occupations, as we know how many categories per group.

K-means and MiniBatch K-means algorithms implementation had been used for this project,
I think they are one of the most used algorithms for clustering. MiniBatch is basically
the same as K-means just with the difference that offers ability to process documents
in batches, that help when the amount of documents is really big and we don't have enough
memory.

After clustering script finished we got an insight of how our data is distribuited,
how each group is conformed and the similarity between documents, in the next image
we can see some stats of how K-Means behave over our documents. We can see ten top terms
for each cluster.

![Clustering results](images/kmeans.png)

### How to use

Run clustering over documents in a directory:

```python clustering/kmeans.py -d path/to/directory```

It will output something like the image above.

If we want to save our model we can pass option ```-o path/to/output/file```, so we can
reuse our model.

## Classifier

Main goal is to train a model that is able to classify a job description document
into one of the major groups indicated by **SOC** system, this require some try-and-error
practice to adjust parameters for each classification algorithm until we find the best
match for our project, sklearn provide a good set of options to pick. Also beside
find the right parameters we need to set the rules for preprocess text and convert it
to numeric vectors, in this case we use **stemming**, **ngrams** and **TF-IDF** weighting.

![Classifiers performance](images/classifiers_graph.png)

**LinearSVC** was the model that give us the best score, it is part of
**Support Vector Machines** package (*sklearn.svm*) provided by **sklearn** toolkit. It
is from the **SVC family** which can use a variety of kernels, being the linear
kernel the one specifically used by LinearSVC.

![SVC Kernels](images/kernels.png)

We still have chance to increase obtained scores, because as we see in the image below, there
is a couple of categories that are getting poor scoring (*11-0000, 43-0000*), and
others that definetely can be improved. As annotated before, this is caused by our
gold dataset not being 100% reliable at this point, need to work more time on those
specific categories to clean, and probably replace, documents included there.

![Best classifier](images/best_classifier.png)

For education purposes and as a basic setup to apply knowledge obtained in course, I
will say this scoring is not bad, but can be improved.

A better and deeper classification is planned for the future, including  better score
and classification in the differents layer of SOC, until get a specific occupation.
This model doesn't return the probability of beign selected for certain category, not
sure if that will be required in a future, if so, instead of *LinearSVC* we need to train
**SVC** with different kernel and probability enabled, having in mind next observation:

> SVMs do not directly provide probability estimates, these are calculated using an
> expensive five-fold cross-validation.

There is always the option to try new classification algorithms, just need to play with
optimization parameters to figure out which could be the best setup.

### How to use

1. Extract compressed file: ```tar xvfz onet.tgz```
2. Run model training: ```python classifier/onet_model.py```


## Classification API

The classification module is the end-user layer that provides a simple API, powered by Django,
which use our trained model to predict and return SOC category for a document. For educational
purposes this API won't go into details to set authentication/authorization resources, it will
just process any requests.

## How to Use

To use this project, follow these steps:

Into project directory:

1. ```pip install -r requirements.txt```
2. ```python manage.py migrate```
3. ```python manage.py runserver```
4. Initiate a python console
5. Console commands:
```python
>>> import requests

>>> api_url = 'http://localhost:8000/classification/jobs/classify/'
>>> document = 'job description document to be classified'
>>> reference_number = 'document_identifier'

>>> payload = { 'content': document, 'reference_number': reference_number }
>>> r = requests.post(api_url, data=payload)

>>> print(r.text)
```

Check API predictions in dashboard:
[API Dashboard](http://127.0.0.1:8000/classification/jobs/)

## Deployment to Heroku

    $ heroku create
    $ git push heroku master

    $ heroku run python manage.py migrate

## License: MIT

## Further Reading

- [Scikit Learn](http://scikit-learn.org/stable/index.html)
- [Standard Occupational Classification System](https://www.bls.gov/soc/)
- [O*NET Resource Center](https://www.onetcenter.org/aboutOnet.html)
- [CareerOneStop](https://www.careeronestop.org/)
- [Django](https://docs.djangoproject.com/en/2.0/intro/tutorial01/)
- [Heroku ready-made application](https://github.com/heroku/python-getting-started)
